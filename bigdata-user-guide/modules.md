% :title: Modules: Additional Software
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/modules.html
% :topic: modules
% :keywords: MODULES, SOFTWARE, HADOOP, DISTRIBUTION, CDH, PYTHON, ANACONDA3, ANACONDA2, SPARK, SPARKLYR, MAVEN, SBT, CENTOS, TUTORIAL, LMOD, DOCUMENTATION
% :content:

(modules)=
# Modules: Additional Software

Similarly to how it is done in the FT supercomputer, the modules allow you to load additional software that is not included by default in the Hadoop distribution that we are using to deploy the platform (in our case CDH 6.1.1).

It also allows to load different versions of the tools than the ones included in the platform.

You can check the available software using:

```
module available
```

For example, using modules you can load Python 3 that is not officially supported neither by Centos 7 nor by CDH 6. To load it you can run:

```
module load anaconda3/2020.02
```

This is the current list of available modules:

- spark
- sparklyr
- anaconda3
- anaconda2
- maven
- sbt

For further information on how to use modules you can check the [Modules Tutorial] that we have prepared to get you started, and the official [Lmod documentation].

[lmod documentation]: https://lmod.readthedocs.io/en/stable/
[modules tutorial]: https://bigdata.cesga.es/tutorials/modules.html
