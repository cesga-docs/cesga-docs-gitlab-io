% :title: How to upload data
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/how_to_upload_data.html
% :topic: how_to_upload_data
% :keywords: SCP, GRIDFTP, GLOBUS ONLINE, DTN SERVER, DTN.SRV.CESGA.ES, CESGA#DTN
% :content:

(how_to_upload_data)=
# How to upload data

Depending on the size of the data that you want to upload you have different options:

- For small amounts of data (\<10GB) you can use scp directly.
- For large amounts of data it is recommended that you use the GridFTP service through Globus Online.

In all cases it is recommended that you do the transfer against our DTN server: **dtn.srv.cesga.es**, this will give you a much better network performance. In Globus Online the endpoint of this server is: **cesga#dtn**.

% note: If you want to transfer large amounts of data we recommend to use Globus Online, just let us know and we will help you with the transfer.

:::{figure} _static/screenshots/globus-dtn.png
:align: center

Using Globus Online to transfer files.
:::
