% :title: How to connect
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/how_to_connect.html
% :topic: how_to_connect
% :keywords: SSH, VPN, HADOOP, CLUSTER, REMOTE DESKTOP, VISUALIZATION PLATFORM, SSH CLIENT, EDGE NODES, WEBUI, LOGIN, HUE PORTAL
% :content:

(how_to_connect)=
# How to connect

:::{warning}
The VPN must be active.
:::

Before connecting, remember to start the VPN or connect from a remote desktop of the visualization platform. This will allow you to access the internal addresses of the cluster.
If you need to setup the VPN you can check the {ref}`VPN` section.

The most common way to connect is using an SSH client to log in to the cluster edge nodes:

```
ssh hadoop3.cesga.es
```

The cluster also has several web user interfaces available, you can access them through the <https://bigdata.cesga.es> WebUI Login option. You can find useful for example the link that you have there to the **HUE** portal, that allows you to use the Hadoop cluster from a graphical interface.
