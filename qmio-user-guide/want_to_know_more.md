(qmio_want_to_know_more)=

% :title: Further assistance
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/want_to_know_more.html
% :topic: want_to_know_more
% :keywords: SISTEMAS, CESGA, EMAIL, LOGIN, FILESYSTEMS, QUOTA, QUEUE, LIMITATIONS, REQUIREMENTS, APLICACIONES, DEVELOPMENT, TOOLS, MPI, APPLICATIONS
% :content:

# Further assistance

[sistemas@cesga.es](mailto:sistemas@cesga.es) or calling 981 56
98 10 asking for "Departamento de Sistemas":

- Login and access problems
- Filesystems quota
- Queue system problems and limitations
- Special requirements

[aplicaciones@cesga.es](mailto:aplicaciones@cesga.es) or calling 981 56
98 10 asking for "Departamento de Aplicaciones":

- Development tools
- MPI problems
- Applications use
