% :title: Intro to the cluster
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/intro_to_the_cluster.html
% :topic: intro_to_the_cluster
% :keywords: QUANTUM EMULATION, QUANTUM COMPUTATION, HPC, QULACS, FX700, OQC, SUPERCONDUCTING QUANTUM COMPUTER, HYBRID JOBS, CLUSTER, RESEARCH COMMUNITY, DISTRIBUTED COMPUTING.
% :content:

(qmio_intro_to_the_cluster)=
# Intro to the cluster

This cluster was deployed with the main focus of leverage quantum computation and quantum emulation to the next level among research community. You can still execute traditional hpc routines and we are using some well known hpc software to handle resources or software.

The main computation parts of the installation are:

- Quantum Emulation, hardware and software based on a distributed version of Qulacs and FX700 machines from Fujitsu.
- Pure Quantum Computation with OQC 30X generation superconducting quantum computer.
- Hybrid jobs using HPC nodes in addition to the Quantum Emulation/Quantum Computation infrastructure.

As the installation is experimental and in continuous development, you should expect a quick development cycle. Perhaps, your intended workflow is not yet supportes and for this very reason, we will be working with you to be able to deploy your workloads in an efficient way in the cluster.

This guide acts as a good starting point to gain knowledge about the cluster to be able to implement your research needs fast and efficient.

- Installation overview
- First steps
- System use
- Examples
- FAQ
- Additional references

:::{Note}
#### Getting Started

To begin using the system, you can start exploring the available tutorials, which include examples and step-by-step guidance for using the Qmio QPU.

Access the available tutorials here: [Qmio tutorial](https://github.com/javicacheiro/qmio-tutorial-ibergrid).
:::
