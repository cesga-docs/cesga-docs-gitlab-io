(ft3_glossary)=

% :title: None
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/glossary.html
% :topic: glossary
% :keywords: HPC, CACHE, COMPILE, COMPUTE NODE, CORE, CORE HOUR, CPU, DIRECTORY, DISK, DISTRIBUTED-MEMORY JOB, DISTRIBUTED-MEMORY PROGRAMMING, ENVIRONMENT MODULE, ENVIRONMENT VARIABLES, FILESYSTEM, FILE/DIRECTORY PERMISSION, GPU, HARDWARE THREAD, JOB, JOB SUBMISSION SCRIPT, JOB SCHEDULER, JOB PRIORITY, MPI, OPERATING SYSTEM, OPENMP, QOS, PARALLEL JOB, PROGRAM, RAM, RESOURCE REQUIREMENTS, SCALABILITY, SCHEDULING, SERIAL JOB, SHARED-MEMORY JOB, SHARED-MEMORY PROGRAMMING, SHELL, SLURM, STEP, SSH, TASK, TERMINAL
% :content:

# HPC Glossary

Just in case you get lost in the HPC language, here are some words you should know but first, let's define HPC itself:

:::{Note}
HPC means **High-Performance Computing** and it involves the use of computer clusters to become a supercomputer. A HPC cluster is a collection of servers called nodes which are interconnected by a really fast network. The typical architecture of a cluster is based on one or two management nodes, 1-4 login nodes, a large collection of compute nodes and also some nodes for storage use. Users of HPC access to the cluster by the login nodes, submit their jobs and the job scheduler sends them to any of the compute nodes available with the resources requested.
:::

**Cache:** small and very fast working memory which is part of a processor, used to hold data transferred to or from the RAM memory.

**Compile:** to produce a binary exectuable from human-readable source code written in a programming language.

**Compute node:** it is one of the multiple servers of the cluster dedicated to perform the computations. Basically, they are made up by processors, volatile memory (RAM) and a local persistent storage (disk). There are different types of [compute nodes](https://cesga-docs.gitlab.io/ft3-user-guide/overview.html) in our cluster, FinisTerrae III.

**Core:** one of the subsystems forming a processor, which can independently run instructions that are part of a process. It comprises arithmetic and logic units (ALU), floating-point units (FPU) and some cache memory.

**Core hour:** unit of computing resource usage corresponding to using a full CPU for one hour.

**CPU:** Central Processing Unit. In general, CPU and processor are synonyms but in this context, a CPU is a single allocatable unit for computation and it corresponds to a core.

**Directory:** also called folder in some contexts. It's a cataloging structure that contains files or other directories. In our cluster, each user has a [HOME](https://cesga-docs.gitlab.io/ft3-user-guide/storage.html) directory where they can write and create subdirectories. It is the default current working directory when users connect to the FinisTerrae III. The users also have access to the [STORE](https://cesga-docs.gitlab.io/ft3-user-guide/storage.html) and [LUSTRE](https://cesga-docs.gitlab.io/ft3-user-guide/storage.htm) directories.

**Disk:** persistent storage attached to a node.

**Distributed-memory job:** jobs whose tasks are running in parallel have their own memory space. The jobs can be spread across the compute nodes.

**Distributed-memory programming:** parallel programming paradigm where processes exchange messages rather than sharing memory. If the exchange mechanism is able to travel through networks, as is the case with [MPI](https://cesga-docs.gitlab.io/ft3-user-guide/parallelization.html), the processes can live in different nodes.

**Environment module:** allows modifying the environment in which programs are started by the shell. Modifying variables such as `$PATH` or `$LD_LIBRARY_PATH`, users can choose which of the [installed software](https://cesga-docs.gitlab.io/ft3-user-guide/env_modules.html) they want to use.

**Environment variables:** a shell variable holding values which can be specified by the user, by the environment module system or by SLURM, that can alter the way sofware behaves.

**Filesystem:** data structure mechanism to store and retrieve files. A filesystem local to a compute node can only be accessed from that node. By contrast, a network filesystem (e.g. NFS) is hosted on a storage node and exported to other nodes.

**File/directory permission:** metadata associated with a file or directory that defines what type of access (read/write/execute) can the user have.

**GPU:** Graphical Processing Unit used as an [accelerator](https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html) to speed up computational times. It's use requieres specific libraries such as CUDA.

**Hardware thread:** independent instruction processing channel inside a core. All threads in a core share some of the computation units of the core.

**Job:** sequence of instructions or steps that can be performed sequentially or in parallel. A job is described by a job submission script and submitted to the job scheduler, which will allocate the resources needed for that job.

**Job submission script:** is a text file or [script](https://cesga-docs.gitlab.io/ft3-user-guide/batch_examples.html) containing shell commands that describes the steps a job must follow (what commands to run, which program to start...) along with its resource requirements and any other additional parameters.

**Job scheduler:** in charge of accepting, scheduling and dispatching jobs based on their resources requests and priority.

**Job priority:** number associated with a job that is used to decide the ordering in which jobs are considered for resource allocation and to start its execution. The priority can be computed based on multiple criteria such as memory, time, job size, jog age, queue time, QOS...

**MPI:** Message Passing Interface. De-facto standard for distributed-memory programming.

**Operating system:** software that runs the base services on a node and connects the hardware resources with the software processes.

**OpenMP:** standard for shared-memory programming abstracts explicit threads and offer parallel programming constructs such as a parallel for loop.

**QOS:** [Quality of service.](https://cesga-docs.gitlab.io/ft3-user-guide/batch_qos.html) Limits associated to the partitions, queues or users.

**Parallel job:** job whose steps consist in either multiple tasks (distributed-memory job) or a single tasks using multiple CPUs (shared-memory job). How well a parallel job performs is called scalablilty.

**Program:** sequence of instructions in either human-readable format (source code) or machine-readable (compiled format or binary).

**RAM:** Random Access Memory. The main working memory of a node, holding the data produced or consumed by the computation process during its lifetime.

**Resource requirements:** list of resources that are needed by a job such as number of CPUs, amount of RAM memory, possibly GPUs and a maximum duration for the usage of the those resources. In FinisTerrae III the only mandatory parameters are [time and memory.](https://cesga-docs.gitlab.io/ft3-user-guide/batch_memory.html)

**Scalability:** measures how well a job or program can use increasing amounts of computing resources. A program that strongly scales takes less and less time as more computing power is used. A program that weakly scales takes roughtly the same time when more data and more computing power are used in the same proportions.

**Scheduling:** action of deciding which job can use which resources and when, based on resource availability and job priorities.

**Serial job:** job whose steps consist in only one task using one CPU.

**Shared-memory job:** job whose parallel threads or processes can all share the same memory or at least a common portion of memory. Such jobs can only run on a single node.

**Shared-memory programming:** parallel programming paradigm where all threads or processes are able to read and/or write in the same memory space.

**Shell:** command-line interface that interprets user commands. It is the way to communicate with the system.

**SLURM:** open source, fault-tolerant and highly scalable cluster management and [job scheduling system](https://cesga-docs.gitlab.io/ft3-user-guide/batch_system.html) for Linux clusters. It is the one used in FinisTerrae III.

**Step:** invocation of a program along with its arguments through the `srun` command. It will spawn as many tasks as requested.

**SSH:** [Secure Shell.](https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html) Protocol by which users can connect to our cluster using their credentials.

**Task:** running instance of a program started by `srun`. Multiple tasks of the same step will run in parallel on similar or distinct nodes. It can be using multiple CPUs in parallel.

**Terminal:** manages the interactions between the shell and the console of the user. The shell can be local to the machine or running on a remote server and connected to through SSH.
