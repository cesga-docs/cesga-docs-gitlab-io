Dissemination and publicity
===========================

For any user which may conduct any research project using the CESGA's services, our center must be properly cited as a collaborating center and mentioned in the acknowledgments section. 

We offer a version in Spanish, Galician and English that should be used. While modifications on the text are permitted, it is essential that the **minimum information** outlined below remains intact:

**English version:**

> This research project was made possible through the access granted by the Galician Supercomputing Center (CESGA) to its supercomputing infrastructure. The supercomputer FinisTerrae III and its permanent data storage system have been funded by the NextGeneration EU 2021 Recovery, Transformation and Resilience Plan, ICT2021-006904, and also from the Pluriregional Operational Programme of Spain 2014-2020 of the European Regional Development Fund (ERDF), ICTS-2019-02-CESGA-3, and from the State Programme for the Promotion of Scientific and Technical Research of Excellence of the State Plan for Scientific and Technical Research and Innovation 2013-2016 State subprogramme for scientific and technical infrastructures and equipment of ERDF, CESG15-DE-3114


**Galician version:**

> Este proxecto de investigación foi posible grazas ao acceso garantido polo Centro de Supercomputación de Galicia as súas infraestruturas de supercomputación. O superordenador FinisTerrae III e o seu sistema de almacenamento permanente de datos foron financiados polo NextGeneration EU 2021 Plan de Recuperación, Transformación e Resiliencia, ICT2021-006904, e tamén co Programa Operativo Plurirregional de España 2014-2020 do FEDER (Fondo Europeo de Desenvolvemento Rexional), ICTS-2019-02-CESGA-3, e co Programa Estatal de Fomento da Investigación Científica e Técnica de Excelencia do Plan Estatal de Investigación Científica e Técnica e de Innovación 2013-2016 Subprograma estatal de infraestructuras científicas e técnicas e equipamiento do FEDER, CESG15-DE-3114.


**Spanish version:**

> Este trabajo de investigación se desarrolló gracias al acceso concedido por el Centro de Supercomputación de Galicia (CESGA) a sus infraestructuras de supercomputación. El Superordenador FinisTerrae III y su sistema de almacenamiento permanente de datos han sido financiados por el NextGeneration EU 2021 Plan de Recuperación, Transformación y Resiliencia, ICT2021-006904, y también con el Programa Operativo Plurirregional de España 2014-2020 del FEDER (Fondo Europeo de Desenvolvemento Rexional), ICTS-2019-02-CESGA-3, y con el Programa Estatal de Fomento da Investigación Científica y Técnica de Excelencia del Plan Estatal de Investigación Científica y Técnica y de Innovación 2013-2016 Subprograma estatal de infraestructuras científicas y técnicas y equipamiento del FEDER, CESG15-DE-3114.

