(ft3_compilers_and_dev_tools)=

% :title: Applications, compilers and development tools
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/compilers_and_dev_tools.html
% :topic: compilers_and_dev_tools
% :keywords: APPLICATIONS, COMPILERS, DEVELOPMENT TOOLS, AREA_COMPILER, AREA_MPI, BIOINFORMATICS, CHEMISTRY, MATERIALS, EDITOR, MACHINE LEARNING, MULTIPHYSICS, CFD, MATH LIBRARY, PROFILING, SCIENTIFIC ANALYSIS, SIMULATION, SOFTWARE MANAGEMENT, TOOL, VISUALIZATION, DATA FORMATS, INTEL ONEAPI HPC TOOLBOX, GNU COMPILERS, OPENMPI
% :content:

# Applications, compilers and development tools

**- Applications:** CESGA provides a wide variety of pre-built applications, optimized for Finisterrae III. The primary way these are provided in the session environment is through modules. Please use the command `module spider` to see the complete list of available applications or refer to the [Environmental modules](https://cesga-docs.gitlab.io/ft3-user-guide/env_modules.html) section.

Applications are grouped into the following areas:

- Comp: Compiler (key: **area_compiler**)
- MPI: MPI library (key: **area_mpi**)
- Bio: Bioinformatics (key: **area_bioinformatics**)
- Chem: Chemistry and Materials (key: **area_chemistryandmaterials**)
- Ed: Editor (key: **area_editor**)
- ML: Machine Learning (key: **area_machinelearning**)
- MPCFD: Multiphysics and CFD (key: **area_multiphysicsandcfd**)
- Math: Math Library (key: **area_mathlibrary**)
- Prof: Profiling (key: **area_profiling**)
- ScA: Scientific Analysis (key: **area_scientificanalysis**)
- Sim: Simulation (key: **area_simulation**)
- Soft: Software Management (key: **area_softwaremanagement**)
- Tool: Tool (key: **area_tools**)
- VisF: Visualization and data formats (key: **area_visualizationanddataformats**)

Using the key associated with each area, it is possible to search for the applications grouped in it using the `key` option of the `module` command. The full list of applications available can be found at [Cesga website](https://www.cesga.es/en/infrastructures/applications/) or at [available modules](https://cesga-docs.gitlab.io/ft3-user-guide/env_modules.html#id1) section.

**- Compilers:** Initially the recommended compilers for the C, C++, and Fortran languages are the included on the [Intel® oneAPI HPC toolbox 2021.3](https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html). This suite will be updated as new versions will be released. Also GNU compilers are available in different versions.

**- MPI:** Intel MPI is included in the [Intel® oneAPI HPC toolbox 2021.3](https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html). Different OpenMPI versions (started from 4.0.5 version) are also available.

# EESSI

The European Environment for Scientific Software Installations ([EESSI](https://www.eessi.io/docs/)), pronounced as "*easy*" is a collaboration between different European partners in HPC community. The goal of this project is to build a common stack of scientific software installations for HPC systems and beyond, including laptops, personal workstations and cloud infrastructure.

Finisterrae 3 has available this stack to all our users.

To enable it, run:

```
module --force purge
source /cvmfs/software.eessi.io/versions/2023.06/init/bash
```

Then, with the usual Lmod commands, you can query and load the available modules (module load, module spider, etc.).

Check the available software: [https://www.eessi.io/docs/available_software/overview/](https://www.eessi.io/docs/available_software/overview/)

