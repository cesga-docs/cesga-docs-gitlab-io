(ft2_compilers_and_dev_tools)=

% :title: Compilers and development tools
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/compilers_and_dev_tools.html
% :topic: compilers_and_dev_tools
% :keywords: COMPILERS, DEVELOPMENT TOOLS, MODULE SPIDER, INTEL PARALLEL STUDIO XE, GNU COMPILERS, MPI, OPENMPI
% :content:

# Compilers and development tools

La lista más actualizada de aplicaciones será la disponible mediante el
comando “**\*module spider\***”. Inicialmente está disponible el
Intel® Parallel Studio XE 2016. El objetivo será tener disponible todas
las versiones consecutivas de esta suite. Así mismo están disponibles
los compiladores de GNU en diferentes versiones.

En cuanto al MPI, aparte del Intel MPI, estará disponible una versión
actualizada de OpenMPI.
