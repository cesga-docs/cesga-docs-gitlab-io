(ft2_partitions)=

% :title: Partitions
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_partitions.html
% :topic: batch_partitions
% :keywords: PARTITIONS, JOBS, NODES, CORES, PARALLEL, THIN-SHARED, THINNODES, EXCLUSIVE, GPU-SHARED-K2, SHARED, AMD-SHARED, CL-INTEL-SHARED
% :content:

# Partitions

The 316 Finis Terrae II nodes are grouped into partitions that are
logical sets of nodes to which jobs can be submitted.

By default, all jobs are sent to the **thin-shared** partition
made up of a small number of standard nodes. The use is
shared between different jobs demanding a number of cores less than those available in a node (24).
For parallel jobs demanding multiple nodes the **thinnodes** should be used.
It is formed by standard nodes with exclusive use, so only one job can run on each node simultaneously.
To submit jobs to these nodes, the option **-p thinnodes** must be used.

Additional integrated nodes are grouped into their own partitions
(**shared**, **gpu-shared-k2**, ...), so to use these nodes
you will need to specify the desired partition with the **-p** option
indicated above.

The same happens with AMD nodes, which are grouped in the partition
**amd-shared**, or with CL-INTEL nodes grouped in the partition
**cl-intel-shared**.
