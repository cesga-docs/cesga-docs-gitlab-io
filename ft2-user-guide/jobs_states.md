(ft2_jobs_states)=

% :title: Jobs states
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/jobs_states.html
% :topic: jobs_states
% :keywords: CA CANCELLED, CD COMPLETED, CF CONFIGURING, CG COMPLETING, F FAILED, NF NODE_FAIL, PD PENDING, PR PREEMPTED, R RUNNING, RS RESIZING, S SUSPENDED, TO TIMEOUT
% :content:

# Jobs states

Esta es el la lista de estados en los que puede aparecer un trabajo:

- CA CANCELLED: El trabajo ha sido explícitamente cancelado por el
  usuario o el administrador del sistema. El trabajo puede ser que no
  hubiera sido iniciado.
- CD COMPLETED: El trabajo ha terminado todos los procesos en los
  nodos.
- CF CONFIGURING: Al trabajo se la han asignado unos recursos, pero
  está a la espera de ellos para poder comenzar. (e.g. booting).
- CG COMPLETING: El trabajo está en el proceso de ser completado.
  Algunos procesos en algunos nodos todavía están activos.
- F FAILED: Trabajo terminado con código de salida distinto de cero u
  otra condición de fallo.
- NF NODE_FAIL: Trabajo cancelado debido al fracaso de uno o varios
  nodos asignados.
- PD PENDING: El trabajo está a la espera de una asignación de
  recursos.
- PR PREEMPTED: Trabajo cancelado debido a un cambio en la prioridad.
- R RUNNING: El trabajo actual tiene una asignación.
- RS RESIZING: El trabajo actual está a punto de cambiar de tamaño.
- S SUSPENDED: El trabajo tiene una asignación, pero la ejecución ha
  sido suspendida.
- TO TIMEOUT: Trabajo cancelado por superar el tiempo límite.
