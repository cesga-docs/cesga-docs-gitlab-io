(ft2_job_monitoring)=

% :title: Jobs monitoring
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_job_monitoring.html
% :topic: batch_job_monitoring
% :keywords: SQUEUE, SCANCEL, JOB_ID, SLURM, SACCT, SQSTAT, SSH, NODES, STATE, JOBS, PARTITIONS
% :content:

# Jobs monitoring

Ver trabajos en cola:

**\*\$ squeue\***

Cancelar un trabajo:

**\*\$ scancel \<job_id>\***

La salida estándar del trabajo se almacena en el fichero
**\*slurm-\<job_id>.out\***

Consulta del histórico de trabajos:

**\*\$ sacct\***

Ver un resumen del estado de todas las particiones y trabajos:

**\*\$ sqstat\***

Es posible conectarse por ssh a los nodos en los que se está ejecutando
un trabajo para ver el estado del trabajo u otras tareas.
