(ft2_batch_email_warning)=

% :title: Email warning
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_email_warning.html
% :topic: batch_email_warning
% :keywords: SBATCH, EMAIL, WARNING, MAIL-TYPE, MAIL-USER, SLURM, FT2.CESGA.ES, BEGIN, END, FAIL, ALL, TIME_LIMIT_50, TIME_LIMIT_80, TIME_LIMIT_90, TIME_LIMIT, SRUN, HOSTNAME, JOB.SH
% :content:

# Email warning

A la hora de enviar un trabajo podemos estar interesados en saber cuando
empieza, cuando termina, si ha fallado, etc, y queremos ser notificados
a nuestros correos, para ello se hace con dos parámetros **--mail-type**
y **--mail-user**, los cuales sirven para definir el tipo de información
que queremos que nos envíe al correo y el correo al que queremos que nos
llegue el aviso, el correo que nos responderá en nuestro caso será
**slurm@ft2.cesga.es**

parámetros de --mail-type: BEGIN, END, FAIL, ALL, TIME_LIMIT_50,
TIME_LIMIT_80, TIME_LIMIT_90, TIME_LIMIT

Ejemplo de uso en un script:

**#!/bin/sh**

**#SBATCH -N 2 #(solicita dos nodos)**

**#SBATCH -n 2 #(dos tareas en total)**

**#SBATCH -t 00:00:30 #(30 sec ejecución)**

**#SBATCH \*\*\***--mail-type=begin\*\*\*\*\* #Envía un correo cuando el
trabajo inicia\*\*

**#SBATCH \*\*\***--mail-type=end\*\*\*\*\* #Envía un correo cuando el
trabajo finaliza\*\*

**#SBATCH \*\*\***<mailto:--mail-user=tec_sis1@domain.com***>\*\* #Dirección a la
que se envía\*\*

**srun hostname**

\# sbatch ./job.sh

Submitted batch job 16899

El resultado que llegará al correo tras el envío del script anterior
será el mostrado aquí abajo:

:::{figure} _static/screenshots/batch_email_warning.png
:align: center
:::
