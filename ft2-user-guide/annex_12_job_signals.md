(ft2_job_signals)=

% :title: Annex XII: Job signals
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_12_job_signals.html
% :topic: annex_12_job_signals
% :keywords: SLURM, SBATCH, SIGNAL, USR1, TERM, TRAP, CLEANUP, FUNCTION, DATE, SLEEP, WAIT, SCANCEL, SLURM CONFIGURATION, JOB MANAGEMENT, BATCH SHELL.
% :content:

# Annex XII: Job signals

Cuando un trabajo llega al tiempo solicitado es matado por el sistema de
colas, pero puede darse el caso de que llegados a esa situación, pueda
interesarnos limpiar el directorio de trabajo o recuperar archivos para
reiniciar el trabajo sin perder todo lo hecho hasta ese momento, o
incluso, guardar alguna información que nos permita relanzar el trabajo
y que empiece donde se había quedado.

Utilizando la opción **--signal** del comando *sbatch* podemos
indicarle a nuestro trabajo que antes de llegar al límite del tiempo,
haga ciertas tareas que nos interesan.

> **--signal=\[B:\]\<sig_num>\[@\<sig_time>\]**

When a job is within sig_time seconds of its end time, send it the
signal sig_num. Due to the resolution of event handling by Slurm, the
signal may be sent up to 60 seconds earlier than specified. sig_num may
either be a signal number or name (e.g. "10" or "USR1"). sig_time must
have an integer value between 0 and 65535. By default, no signal is sent
before the job's end time. If a sig_num is specified without any
sig_time, the default time will be 60 seconds. Use the "B:" option to
signal only the batch shell, none of the other processes will be
signaled. By default all job steps will be signalled, but not the batch
shell itself.

En este ejemplo, le pedimos a Slurm que envíe una señal a nuestro script
120 segundos antes de que se agote el tiempo de espera para darnos la
oportunidad de realizar acciones de limpieza.

El primer paso será incluir la opción **--signal** para definir que
queremos enviar una señal (*USR1*) al trabajo un tiempo (120 segundos)
antes de llegar al límite del tiempo. Debemos tener en cuenta que debido
a la gestión de los eventos del Slurm, esta señal puede ser enviada
hasta 60 segundos antes del tiempo indicado. Por otro lado, este tiempo
indicado debe ser el suficiente para realizar todas las tareas que
deseemos pues una vez finalizado el tiempo, el trabajo se matará si ó
si.

#!/bin/bash

…

\# asks SLURM to send the USR1 signal 120 seconds before the end of the
time limit

#SBATCH --signal=B:<mailto:USR1@120>

El siguiente paso puede ser la definición de una función en la cual
llevaremos a cabo todas las tareas necesarias según lo que queramos
hacer.

\# define the handler function. Note that this is not executed here, but
rather

\# when the associated signal is sent

your_cleanup_function()

{

echo "function your_cleanup_function called at \$(date)"

\# do whatever cleanup you want here

}

A continuación deberemos indicarle al trabajo que queremos gestionar la
señal indicada anteriormente utilizando la función definida previamente
para ello.

\# call your_cleanup_function once we receive USR1 signal

trap 'your_cleanup_function' USR1

Por último, debemos realizar las tareas de nuestro trabajo, tal y como
veníamos haciendo, con la única salvedad de que debemos añadir un “&” al
final de la tarea o tareas principales del trabajo y finalizar el script
con la función wait. Si no se hace esto, no se atraparan las señales.

echo "starting calculation at \$(date)"

\# the calculation "computes" (in this case sleeps) for 1000 seconds

\# but we asked slurm only for 240 seconds so it will not finish

\# the "&" after the compute step and "wait" are important

sleep 1000 &

wait

Otra señal que nos podría interesar sería la señal **TERM**, la cual
nos permitiría manejar la eliminación de un trabajo con el comando
**scancel**. El problema de esta señal es que, dada la configuración
de slurm, solamente tenemos 30 segundos para llevar a cabo las tareas
deseadas, por lo cual solo nos permitiría hacer pequeñas tareas muy
rápidas.

Ejemplo:

**/opt/cesga/job-scripts-examples/job_signal_timeout.sh**
