(ft2_faqs)=

% :title: FAQs
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/faqs.html
% :topic: faqs
% :keywords: SSH, DNS SPOOFING, RSA HOST KEY, MAN-IN-THE-MIDDLE ATTACK, KNOWN_HOSTS, SSH-KEYGEN, LUSTRE, LFS, SETSTRIPE, QUOTA, SQUEUE, VIM, NEDIT-NG, ATOM, NODE.JS, QT, FILESYSTEM, OST, POOL, STRIPE_SIZE, START_OST_IDX, STRIPE_COUNT, BLOCK, FILE SYSTEM, DIRECTORY, HOME, SPACE, LIMIT, GRACE, QUOTA, LFS QUOTA, MYQUOTA
% :content:

# FAQs

P: Al conectarse por ssh al ft2 aparece el siguiente mensaje:

______________________________________________________________________

@ WARNING: POSSIBLE DNS SPOOFING DETECTED! @

______________________________________________________________________

The RSA host key for [\*ft2.cesga.es\*](http://ft2.cesga.es/) has
changed,

and the key for the corresponding IP address 193.144.35.6

is unknown. This could either mean that

DNS SPOOFING is happening or the IP address for the host

and its host key have changed at the same time.

______________________________________________________________________

@ WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! @

______________________________________________________________________

IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!

Someone could be eavesdropping on you right now (man-in-the-middle
attack)!

It is also possible that the RSA host key has just been changed.

The fingerprint for the RSA key sent by the remote host is

7c:23:f3:e6:7c:f5:6d:68:58:7e:2a:41:03:69:9b:d1.

Please contact your system administrator.

Add correct host key in /xxxx/xxxxx/.ssh/known_hosts to get rid of this
message.

Offending key in /exports/vm.chavez.p/.ssh/known_hosts:6

RSA host key for [\*ft2.cesga.es\*](http://ft2.cesga.es/) has changed
and you have requested strict checking.

Host key verification failed

R: Editar el fichero */xxxxxx/.ssh/known_hosts* y borrar la entrada
correspondiente a [\*ft2.cesga.es\*](http://ft2.cesga.es/) o ejecutar
el comando “*ssh-keygen -R ft2.cesga.es*”.

P: ¿Cómo puedo cambiar el número de stripes de un fichero o directorio
de Lustre?

R:Por defecto, cada fichero se escribe en un único OST. Con el comando
lfs es posible modificar este valor por fichero (lfs setstripe) o por
directorio:

usage: setstripe \[--stripe-count|-c \<stripe_count>\]

> \[--stripe-index|-i \<start_ost_idx>\]
>
> \[--stripe-size|-S \<stripe_size>\]
>
> \[--pool|-p \<pool_name>\]
>
> \[--block|-b\] \<directory|filename>
>
> **stripe_size**: Number of bytes on each OST (0 filesystem default)
>
> Can be specified with **k**, **m** or g (in KB, MB and GB
>
> respectively).
>
> **start_ost_idx**: OST index of first stripe (-1 default).
>
> **stripe_count**: Number of OSTs to stripe over (0 default, -1 all).
>
> **pool_name**: Name of OST pool to use (default none).
>
> **block**: Block file access during data migration.

P: ¿Es posible conectarse a los equipos en los que tengo un trabajo en
ejecución a través del sistema de colas?

R: Sí, simplemente haciendo un ssh a los nodos en los que se encuentra
el trabajo en ejecución. La lista de nodos en los que se está ejecutando
el trabajo se puede obtener con el comando “**\*squeue”\***

P: ¿Cómo puedo saber el espacio libre en mi directorio home?

R: Utilizando el comando **\*quota \***puedes ver el espacio utilizado y
el límite de utilización:

**\*\$ quota -f /mnt/EMC/Home_FT2\***

Disk quotas for user carlosf (uid 12345):

> Filesystem blocks quota limit grace files quota limit grace

10.117.117.2:/Home_FT2

> 2309712 10240000 11264000 49572 100000 101000

Para conocer el espacio utilizado en el LUSTRE, debe usar el comando
“**\*lfs quota\***”.

Para obtener los datos de todos los directorios que tiene a su
disposición puede usar el comando **\*myquota\***.

P: ¿Qué editores de texto están disponibles?

R: **\*\$ module key area_editor\***

- vim-X11 (gvim)
  ([\*http://vimdoc.sourceforge.net/htmldoc/gui_x11.html\*](http://vimdoc.sourceforge.net/htmldoc/gui_x11.html)):
  module load vim-X11

  - Pros: Muy rápido (soporta muy bien la carga de ficheros grandes de
    : varios cientos de MB) y con toda la potencia del vi. Bien
      mantenido.
  - Contras: quizás complejo aunque ha mejorado mucho su uso

- NEdit-ng
  ([\*https://github.com/eteran/nedit-ng\*](https://github.com/eteran/nedit-ng)):
  module load nedit-ng

  - Pros: Muy rápido (soporta muy bien la carga de ficheros grandes de
    : varios cientos de MB). Bastante potente y personalizable.
  - Contras: es un proyecto reciente que porta NEdit a Qt. La versión
    : es ya estable y utilizable pero es un proyecto en evolución.

- atom ([\*https://atom.io/\*](https://atom.io/)) : module load atom

  - Pros: Editor moderno y muy personalizable desarrollado con
    : node.js. Bien mantenido por una comunidad grande
  - Contras: No soporta bien la carga de ficheros grandes
